var express = require('express');
const { list, index, create, replace, update, destroy } = require('../controllers/bookings');
var router = express.Router();

/* GET users listing. */
router.get('/', list);

// GET users listing.
router.get('/:id', index);

//POST users listing.
router.post('/', create);

//PUT users listing.
router.put('/:id', replace);

//PATCH users listing.
router.patch('/:id', update);

//DELETE users listing.
router.delete('/:id', destroy);

module.exports = router;
