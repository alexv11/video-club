FROM node 
MAINTAINER Alex Villalba
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD ["npm", "start"]