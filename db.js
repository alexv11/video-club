const Sequelize = require('sequelize');

const genreModel = require('./models/genre');
const movieModel = require('./models/movie');
const directorModel = require('./models/director.js');
const memberModel = require('./models/member.js');
const bookingModel = require('./models/booking.js');
const copyModel = require('./models/copy.js');

const sequelize = new Sequelize('video_club', 'root', 'secret', {
    host: '127.0.0.1',
    dialect: 'mysql'
});

const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Director = directorModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Copy = copyModel(sequelize, Sequelize);

// Un género puede tener muchas películas
Genre.hasMany(Movie, { as: 'movies' });

// Una película pertenece a un género	
Movie.belongsTo(Genre, {
    as: 'genre',
});

// Un director puede tener muchas películas
Director.hasMany(Movie, { as: 'movies' });

// Una película pertenece a un director
Movie.belongsTo(Director, {
    as: 'director',
});

// Una copia pertenece a una película
Copy.belongsTo(Movie, {
    as: 'movie',
});

// Una reserva pertenece a una copia
Booking.belongsTo(Copy, {
    as: 'copy',
});

// Una reserva pertenece a un miembro
Booking.belongsTo(Member, {
    as: 'member',
});

sequelize.sync({
    force: true
}).then(() => {
    console.log('Database updated');
});

module.exports = { Genre, Movie, Director, Member, Booking, Copy };

