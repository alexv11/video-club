const express = require('express');
const { Member } = require('../db');

function list(req, res, next) {
    Member.findAll({ 
        include: [movies]
    })
        .then(objects => res.json(objects))
        .catch(err => res.send(err));
};

function index(req, res, next) {
    const id = req.params.id;
    Member.findByPk(id).then(obj => res.json(obj)).catch(err => res.send(err));
};

function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const address = req.body.address;
    const phone = req.body.phone;
    const status = req.body.status;
    
    let Booking = new Object({
        name: name,
        lastName: lastName,
        address: address,
        phone: phone,
        status: status
    });

    Booking.create(booking)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
};

function replace(req, res, next) {
    const id = req.params.id;
    Member.findByPk(id).then(obj => {
        obj.name = req.body.name ? req.body.name : "";
        obj.lastName = req.body.lastName ? req.body.lastName : "";
        obj.address = req.body.address ? req.body.address : "";
        obj.phone = req.body.phone ? req.body.phone : "";
        obj.status = req.body.status ? req.body.status : "";
        obj.update({
                name: name,
                lastName: lastName,
                address: address,
                phone: phone,
                status: status
            })
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
    }).catch(err => res.send(err));
};

function update(req, res, next) {
    const id = req.params.id;
    Member.findByPk(id).then(obj => {       
        obj.name = req.body.name ? req.body.name : obj.name;
        obj.lastName = req.body.lastName ? req.body.lastName : obj.lastName;
        obj.address = req.body.address ? req.body.address : obj.address;
        obj.phone = req.body.phone ? req.body.phone : obj.phone;
        obj.status = req.body.status ? req.body.status : obj.status;
        obj.update({
                name: name,
                lastName: lastName,
                address: address,
                phone: phone,
                status: status
            })
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
    }).catch(err => res.send(err));
};

function destroy(req, res, next) {
    const id = req.params.id;
    Member.destroy({
        where: { id: id }
    })
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
};

module.exports = {list, index, create, replace, update, destroy};

