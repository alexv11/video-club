const express = require('express');
const { Copy } = require('../db');

function list(req, res, next) {
    Copy.findAll({ 
        include: [movies]
    })
        .then(objects => res.json(objects))
        .catch(err => res.send(err));
};

function index(req, res, next) {
    const id = req.params.id;
    Copy.findByPk(id).then(obj => res.json(obj)).catch(err => res.send(err));
};

function create(req, res, next) {
    const number = req.body.number;
    const format = req.body.format;
    const status = req.body.status;

    let Copy = new Object({
        number: number,
        format: format,
        status: status
    });

    Copy.create(copy)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
};

function replace(req, res, next) {
    const id = req.params.id;
    Copy.findByPk(id).then(obj => {
        obj.number = req.body.number ? req.body.number : "";
        obj.format = req.body.format ? req.body.format : "";
        obj.status = req.body.status ? req.body.status : "";
        obj.update({
                number: number,
                format: format,
                status: status
            })
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
    }).catch(err => res.send(err));
};

function update(req, res, next) {
    const id = req.params.id;
    Copy.findByPk(id).then(obj => {
        obj.number = req.body.number ? req.body.number : obj.number;
        obj.format = req.body.format ? req.body.format : obj.format;
        obj.status = req.body.status ? req.body.status : obj.status;
        obj.update({
                number: number,
                format: format,
                status: status
            })
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
    }).catch(err => res.send(err));
};

function destroy(req, res, next) {
    const id = req.params.id;
    Copy.destroy({
        where: { id: id }
    })
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
};

module.exports = {list, index, create, replace, update, destroy};

