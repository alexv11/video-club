const express = require('express');
const { Booking } = require('../db');

function list(req, res, next) {
    Booking.findAll({ 
        include: [movies]
    })
        .then(objects => res.json(objects))
        .catch(err => res.send(err));
};

function index(req, res, next) {
    const id = req.params.id;
    Booking.findByPk(id).then(obj => res.json(obj)).catch(err => res.send(err));
};

function create(req, res, next) {
    const date = req.body.date;
    
    let Booking = new Object({
        date: date
    });

    Booking.create(booking)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
};

function replace(req, res, next) {
    const id = req.params.id;
    Booking.findByPk(id).then(obj => {
        obj.date = req.body.date ? req.body.date : "";
        obj.update({
                date: date
            })
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
    }).catch(err => res.send(err));
};

function update(req, res, next) {
    const id = req.params.id;
    Booking.findByPk(id).then(obj => {       
        obj.date = req.body.date ? req.body.date : obj.date;
        obj.update({
                date: date
            })
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
    }).catch(err => res.send(err));
};

function destroy(req, res, next) {
    const id = req.params.id;
    Booking.destroy({
        where: { id: id }
    })
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
};

module.exports = {list, index, create, replace, update, destroy};

