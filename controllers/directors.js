const express = require('express');
const { Director } = require('../db');

function list(req, res, next) {
    Director.findAll({ 
        include: [movies]
    })
        .then(objects => res.json(objects))
        .catch(err => res.send(err));
};

function index(req, res, next) {
    const id = req.params.id;
    Director.findByPk(id).then(obj => res.json(obj)).catch(err => res.send(err));
};

function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;

    let director = new Object({
        name: name,
        lastName: lastName
    });

    Director.create(director)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
};

function replace(req, res, next) {
    const id = req.params.id;
    Director.findByPk(id).then(obj => {
        obj.name = req.body.name ? req.body.name : "";
        obj.lastName = req.body.lastName ? req.body.lastName : "";
        obj.update({
                name: name,
                lastName: lastName
            })
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
    }).catch(err => res.send(err));
};

function update(req, res, next) {
    const id = req.params.id;
    Director.findByPk(id).then(obj => {
        obj.name = req.body.name ? req.body.name : obj.name;
        obj.lastName = req.body.lastName ? req.body.lastName : obj.lastName;
        obj.update({
                name: name,
                lastName: lastName
            })
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
    }).catch(err => res.send(err));
};

function destroy(req, res, next) {
    const id = req.params.id;
    Director.destroy({
        where: { id: id }
    })
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
};

module.exports = {list, index, create, replace, update, destroy};

