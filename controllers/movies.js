const express = require('express');
const { Movie } = require('../db');

function list(req, res, next) {
    Movie.findAll({ include: []}).then(objects => res.json(objects)).catch(err => res.send(err));
};

function index(req, res, next) {
    const id = req.params.id;
    Movie.findByPk(id).then(obj => res.json(obj)).catch(err => res.send(err));
};

function create(req, res, next) {
    const title = req.body.title;
    const genreId = req.body.genreId;

    let movie = new Object({
        title: title,
        genreId: genreId
    });

    Movie.create(movie)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
};

function replace(req, res, next) {
    const id = req.params.id;
    Movie.findByPk(id).then(obj => {
        obj.title = req.body.title ? req.body.title : "";
        obj.update({
                title: title
            })
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
    }).catch(err => res.send(err));
};

function update(req, res, next) {
    const id = req.params.id;
    Movie.findByPk(id).then(obj => {
        obj.title = req.body.title ? req.body.title : obj.title;
        obj.update({
                title: title
            })
            .then(obj => res.json(obj))
            .catch(err => res.send(err));
    }).catch(err => res.send(err));
};

function destroy(req, res, next) {
    const id = req.params.id;
    Movie.destroy({
        where: { id: id }
    })
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
};

module.exports = {list, index, create, replace, update, destroy};

