module.exports = (sequelize, type) => {
    return Booking = sequelize.define('bookings', {
        id: { type: type.INTEGER, primaryKey: true, autoIncrement: true },
        date: type.DATE
    });
}