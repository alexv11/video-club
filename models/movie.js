module.exports = (sequelize, type) => {
    return Movie = sequelize.define('movies', {
        id: { type: type.INTEGER, primaryKey: true, autoIncrement: true },
        title: type.STRING,
    });
}