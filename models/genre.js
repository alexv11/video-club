module.exports = (sequelize, type) => {
    return Genre = sequelize.define('genres', {
        id: {
            type: type.INTEGER, 
            primaryKey: true, 
            autoIncrement: true
        },
        description: type.STRING,
        status: type.BOOLEAN
    });
}