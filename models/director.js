module.exports = (sequelize, type) => {
    return Director = sequelize.define('movies', {
        id: { type: type.INTEGER, primaryKey: true, autoIncrement: true },
        name: type.STRING,
    });
}